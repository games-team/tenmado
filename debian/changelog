tenmado (0.10-5) unstable; urgency=medium

  * Declare compliance with Debian Policy 4.6.2.

 -- Markus Koschany <apo@debian.org>  Mon, 27 Feb 2023 23:17:10 +0100

tenmado (0.10-4) unstable; urgency=medium

  [ Niels Thykier ]
  * Declare the explicit requirement for (fake)root. The tenmado package
    currently requires (fake)root to produce the debs due to static
    non-root:root ownerships in the debs.

  [ Markus Koschany ]
  * Switch to debhelper-compat = 13.
  * Declare compliance with Debian Policy 4.5.1.

 -- Markus Koschany <apo@debian.org>  Sat, 26 Dec 2020 13:50:37 +0100

tenmado (0.10-3) unstable; urgency=medium

  * Switch to compat level 11.
  * Move the package to salsa.debian.org.
  * Declare compliance with Debian Policy 4.2.0.
  * Drop deprecated menu file.
  * Use dh_auto_install instead of $MAKE.

 -- Markus Koschany <apo@debian.org>  Mon, 13 Aug 2018 10:19:40 +0200

tenmado (0.10-2) unstable; urgency=medium

  * New maintainer Debian Games Team. (Closes: #738878)
  * Add myself to Uploaders.
  * wrap-and-sort -sab.
  * Use compat level 9 and require debhelper >= 9.
  * Declare compliance with Debian Policy 3.9.6.
  * Drop build-dependency on dpkg-dev. It is no longer required.
  * Remove Build-Conflicts field.
  * Move the package to Git. Add VCS-fields.
  * Update debian/copyright to copyright format 1.0.
  * Add longtitle to tenmado.menu.
  * tenmado.docs: Do not install Artistic-2 license text anymore because Debian
    distributes Tenmado under the terms of the GNU General Public License 2.
  * Install icons with install files and clean the renamed files with clean
    file. Install tenmado.png to /usr/share/icons/hicolor/48x48/apps.
  * Use dh sequencer in debian/rules. Build with autoreconf and --parallel.
  * Rename 10pointer_to_readme patch to 10pointer_to_readme.patch.
    Add DEP3 header.
  * Add tenmado.desktop file. (Closes: #738043)

 -- Markus Koschany <apo@debian.org>  Thu, 29 Oct 2015 17:33:56 +0100

tenmado (0.10-1) unstable; urgency=low

  * New upstream release
  * the new upstream release fixes the following bug:
    + Add the return value of sdl-config --libs (which usually
      contains a -l flag) to LIBS, not AM_LDFLAGS.  You can now
      use the --as-needed option of binutils-gold ld.  (closes: #639926)
  * debian/control, debian/rules: don't copy config.guess and config.sub
    from autotools-dev because they are unnecessary
  * debian/control, debian/rules: use dh-autoreconf to regenerate
    autoconf/automake files
  * debian/rules, debian/control: set ./configure options with
    dpkg-buildflags --export=configure
  * debian/control: added Build-Depends: dpkg-dev (>= 1.16.1) because
    this package uses dpkg-buildflags
  * debian/rules: added build-arch and build-indep targets
  * debian/control: added Build-Conflicts: autoconf2.13, automake1.4
    just in case
  * debian/control, debian/compat: set debhelper compatibility level to 8
  * debian/control: set Standards-Version: to 3.9.2
  * debian/tenmado.preinst: the transition to a plain text high score file
    do nothing if /usr/games/tenmado is not available
  * debian/source/format: new file, set the source format to 3.0 (quilt)
  * debian/patches/10pointer_to_readme: added a DEP-3 header
  * debian/rules: removed the obsolete --without-libdb3 option of
    ./configure
  * debian/rules: removed useless chmod at the end of binary-arch

 -- Oohara Yuuma <oohara@debian.org>  Thu, 02 Feb 2012 21:44:42 +0900

tenmado (0.8-1) unstable; urgency=low

  * new upstream release
  * new maintainer (closes: #554257)
  * debian/control, debian/rules, debian/tenmado.docs: no longer uses dbs
  * debian/patches/*: removed
  * debian/control: removed the versioned dependency on libraries
    to respect symbols files if available
  * debian/compat: set debhelper compatibility level to 7
  * debian/control: updated the versioned dependency of debhelper
  * debian/control: Build-Depends on autotools-dev because the package
    uses autoconf
  * debian/control: pass CFLAGS and LDFLAGS to configure script
  * debian/control: replaced dh_clean -k with dh_prep
  * debian/tenmado.gnome-menu: removed
  * debian/control: uses Homepage: for the upstream URL
  * debian/copyright: refers to /usr/share/common-licenses/GPL-2 rather
    than /usr/share/common-licenses/GPL because the license is "GPL
    version 2, no other version is allowed"
  * debian/tenmado.prerm: removed because it does nothing
  * debian/watch: new file, supports uscan
  * debian/rules: set Standards-Version: to 3.8.3
  * debian/tenmado.postrm: removes /var/games on purge if it is not in
    the control of dpkg; assumes that a package which uses /var/games but
    leaves it empty until necessary includes /var/games in its .deb
    rather than creates /var/games in its maintainer script (closes: #333410)

 -- Oohara Yuuma <oohara@debian.org>  Mon, 16 Nov 2009 14:39:24 +0900

tenmado (0.7-3) unstable; urgency=low

  * acknowledged NMU
  * the NMU fixed the following bug:
    + no longer uses /usr/X11R6/include/X11/pixmaps/ for the icon image
      (closes: #403938)
  * debian/tenmado.menu: quoted a string
  * debian/tenmado.menu: uses the section Games/Action instead of
    Games/Arcade
  * debian/compat: new file, setting debhelper compatibility level to 5
  * debian/rules: removed the DH_COMPAT setting
  * debian/control: added ${misc:Depends} to Depends:
  * debian/control: Build-Depends: debhelper (>= 5)

 -- Oohara Yuuma <oohara@debian.org>  Mon, 29 Dec 2008 16:37:52 +0900

tenmado (0.7-2.1) unstable; urgency=low

  * Non-maintainer upload.
  * Move tenmado.xpm to /usr/share/pixmaps. (Closes: #403938)

 -- Carsten Hey <c.hey@web.de>  Sat, 27 Dec 2008 18:29:17 +0100

tenmado (0.7-2) unstable; urgency=low

  * debian/rules, debian/control: no longer uses libdb3 to save high scores
    (closes: #289303)
  * debian/tenmado.preinst, debian/tenmado.postinst: restore old high scores
    when upgrading
  * debian/control: updated Standards-Version:
  * happy birthday, L!

 -- Oohara Yuuma <oohara@debian.org>  Fri, 25 Nov 2005 00:00:52 +0900

tenmado (0.7-1) unstable; urgency=low

  * new upstream release

 -- Oohara Yuuma <oohara@debian.org>  Thu, 25 Nov 2004 15:58:19 +0900

tenmado (0.6-2) unstable; urgency=low

  * debian/tenmado.postinst: chown root.games is deprecated, s/./:/
    (closes: #229200)

 -- Oohara Yuuma <oohara@debian.org>  Sun, 25 Jan 2004 10:49:12 +0900

tenmado (0.6-1) unstable; urgency=low

  * new upstream release

 -- Oohara Yuuma <oohara@debian.org>  Mon, 24 Nov 2003 23:09:11 +0900

tenmado (0.5-1) unstable; urgency=low

  * new upstream release
  * debian/tenmado.docs: installs README-ja-utf8 too

 -- Oohara Yuuma <oohara@debian.org>  Thu, 16 Jan 2003 09:16:28 +0900

tenmado (0.4-1) unstable; urgency=low

  * new upstream release
  * debian/tenmado.postrm: added a long comment about /var/games
  * debian/control: set Standards-Version to 3.5.8

 -- Oohara Yuuma <oohara@debian.org>  Sun,  5 Jan 2003 01:05:06 +0900

tenmado (0.3-1) unstable; urgency=low

  * new upstream release
  * debian/control: added libdb3-dev
  * debian/tenmado.postinst, debian/tenmado.postrm: added high score lists
    handling
  * debian/rules: the tenmado binary is now sgid games
  * debian/rules: added DEB_BUILD_OPTIONS "noopt" support
  * debian/control: set Standards-Version to 3.5.7

 -- Oohara Yuuma <oohara@debian.org>  Sat, 21 Dec 2002 23:57:55 +0900

tenmado (0.2-1) unstable; urgency=low

  * new upstream release
  * happy birthday, L!  (well, it was November 25, but anyway)
  * debian/control: build with newer debhelper (to remove the /usr/doc symlink)
    and SDL libraries (for libpng3 transition)
  * debian/rules, debian/tenmado.menu: added xpm icon
  * debian/tenmado.gnome-menu: new file, adding GNOME1 menu

 -- Oohara Yuuma <oohara@debian.org>  Tue, 26 Nov 2002 02:28:33 +0900

tenmado (0.1dbs-2) unstable; urgency=low

  * debian/control: build with new libsdl-image1.2-dev (which uses libpng3)

 -- Oohara Yuuma <oohara@debian.org>  Mon, 22 Jul 2002 02:37:46 +0900

tenmado (0.1dbs-1) unstable; urgency=low

  * built with dbs
  * This is based on version 0.1.  The upstream version number is changed
    from 0.1 to 0.1dbs to overwrite .orig.tar.gz .
  * debian/changelog: removed emacs variable
  * debian/patches/10pointer_to_readme: new file, adding note about README
    to the manpage

 -- Oohara Yuuma <oohara@debian.org>  Fri, 19 Jul 2002 23:12:15 +0900

tenmado (0.1-1) unstable; urgency=low

  * Initial Release.

 -- Oohara Yuuma <oohara@debian.org>  Sun, 14 Jul 2002 04:47:11 +0900
